\begin{align*}
&r_{j}\left(\textbf{h}_{i}|w\right ) = \sum_{l}w_{l}r_{jl}\left ( \textbf{h}_{i} \right )\\
\end{align*}