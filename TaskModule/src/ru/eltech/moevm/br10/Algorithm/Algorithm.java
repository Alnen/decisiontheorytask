package ru.eltech.moevm.br10.algorithm;

import ru.eltech.moevm.br10.algorithm.conflictsolver.ConflictSolver;
import ru.eltech.moevm.br10.algorithm.conflictsolver.IConflictSolver;
import ru.eltech.moevm.br10.algorithm.conflictsolver.RandomConflictSolver;
import ru.eltech.moevm.br10.utility.*;
import ru.eltech.moevm.br10.utility.listener.IStepLogger;

import javax.swing.*;
import java.util.ArrayList;

public class Algorithm extends ListenerHandler<IStepLogger> {
    private final int number_of_6_nodes = 12;
    private ConflictSolver solver_;
    private ArrayList<Node> breadth_first_search_list = new ArrayList<Node>();
    private Node root_node_;

    public Algorithm (Node root, ConflictSolver solver){
        solver_ = solver;
        root_node_ = root;
        reset();
    }

    public Algorithm (Node root) {
        this(root, null);
        solver_ = new RandomConflictSolver();
    }

    public void setSolver(ConflictSolver solver){
        for(IStepLogger handler: getHandlersList()){
            solver.addComponent(handler);
        }
        solver_ = solver;
    }
    public IConflictSolver getSolver () { return solver_; }

    public boolean isFinished(){
        return breadth_first_search_list.size() == 0;
    }

    public void solve () {
        if (!isDataValid()) return;
        while(!isFinished()){
            proceedOneStep();
        }
    }

    public void reset()
    {
        breadth_first_search_list.clear();
        breadth_first_search_list.add(root_node_);
        for (int i =  0; i < breadth_first_search_list.size() ; ++i)
        {
            Node current_root_node = breadth_first_search_list.get(i);
            current_root_node.getValue().setV(0.0);
            current_root_node.getValue().setN(0.0);
            if (current_root_node.getEdgeToParent() != null)
                current_root_node.getEdgeToParent().setPreferred(false);
            for(Node child: current_root_node.getChilds()){
                breadth_first_search_list.add(child);
            }
        }
        for (int i = 0; i < number_of_6_nodes; ++i)breadth_first_search_list.remove(breadth_first_search_list.size()-1);
    }

    public static double round(double value) {
        return (double) Math.round(value * 100) / 100;
    }

    private boolean isDataValid()
    {
        ArrayList<Node> node_stack = new ArrayList<Node>();
        node_stack.add(root_node_);
        while (!node_stack.isEmpty())
        {
            Node current_root_node = node_stack.remove(0);
            for (Node child: current_root_node.getChilds())
            {
                node_stack.add(child);
            }
            if (current_root_node.getType() == NodeType.LOTTERY) {
                double sum = 0.0;
                for (int i = 0; i < current_root_node.getChilds().size(); ++i) {
                    sum += round(current_root_node.getChilds().get(i).getEdgeToParent().getProbability());
                }
                if ((sum < 0.9999999999)||(sum > 1.0000000001)) {
                    JOptionPane.showMessageDialog(null, "Not right input data");
                    return false;
                }
            }
        }
        return true;
    }

    public void proceedOneStep () {
        if (!isFinished() && isDataValid()) {
            Node current_node = breadth_first_search_list.remove(breadth_first_search_list.size() - 1);

            logAction("<p>Обрабатываем узел " + current_node.getName() + ":</p><p style='margin-left: 20px;'>");
            if (current_node.getType() == NodeType.DECISION) {
                if (current_node.getChilds().size() == 2) {
                    logAction("Данный узел является узлом решения.<br>");
                    Value value1 = new Value();

                    value1.add(current_node.getChilds().get(0).getValue());
                    value1.add(current_node.getChilds().get(0).getEdgeToParent().getValue());


                    logAction("Первый выбор - " + current_node.getChilds().get(0).getEdgeToParent().getDescription() + " :<br>");
                    logAction("Стоимость узла " + current_node.getChilds().get(0).getName() + " : ( " + round(current_node.getChilds().get(0).getValue().getV()) + ", " + round(current_node.getChilds().get(0).getValue().getN()) + " )<br>");
                    logAction("Стоимость перехода к узлу " + current_node.getChilds().get(0).getEdgeToParent().getName() + " : ( " + round(current_node.getChilds().get(0).getEdgeToParent().getValue().getV()) + ", " + round(current_node.getChilds().get(0).getEdgeToParent().getValue().getN()) + " )<br>");
                    logAction("Итоговая стоимость решения : ( " + round(value1.getV()) + ", " + round(value1.getN()) + ")");

                    Value value2 = new Value();

                    value2.add(current_node.getChilds().get(1).getValue());
                    value2.add(current_node.getChilds().get(1).getEdgeToParent().getValue());

                    logAction("Второй выбор - " + current_node.getChilds().get(1).getEdgeToParent().getDescription() + " :<br>");
                    logAction("Стоимость узла " + current_node.getChilds().get(1).getName() + " : ( " + round(current_node.getChilds().get(1).getValue().getV()) + ", " + round(current_node.getChilds().get(1).getValue().getN()) + " )<br>");
                    logAction("Стоимость перехода к узлу " + current_node.getChilds().get(1).getEdgeToParent().getName() + " : ( " + round(current_node.getChilds().get(1).getEdgeToParent().getValue().getV()) + ", " + round(current_node.getChilds().get(1).getEdgeToParent().getValue().getN()) + " )<br>");
                    logAction("Итоговая стоимость решения : ( " + round(value2.getV()) + ", " + round(value2.getN()) + ")");

                    if (value1.isComparable(value2)) {
                        if (value1.isBiggerThan(value2)) {
                            logAction("Т.к стоимость первого решения больше стоимости второго решения, стоимость узла "
                                    + current_node.getName() + " становится равной ( " + round(value1.getV()) + ", " + round(value1.getN()) + " )</p>");
                            current_node.getChilds().get(0).getEdgeToParent().setPreferred(true);
                            current_node.setValue(value1);
                        } else {
                            logAction("Т.к стоимость второго решения больше стоимости первого решения, стоимость узла "
                                    + current_node.getName() + " становится равной ( " + round(value2.getV()) + ", " + round(value2.getN()) + " )</p>");
                            current_node.getChilds().get(1).getEdgeToParent().setPreferred(true);
                            current_node.setValue(value2);
                        }
                    } else {
                        logAction("Т.к стоимости решений не сравнимы, то решение будем принимать исходя из выбранной нами стратегии решения конфликтов(" + solver_.getTypeString() + "):<br>");

                        Challenger challenger_a = new Challenger(current_node.getChilds().get(0), value1);
                        Challenger challenger_b = new Challenger(current_node.getChilds().get(1), value2);
                        Challenger winner = solver_.chose(challenger_a, challenger_b);

                        logAction("<br>Стоимость текущего узла становится равной ( " + round(winner.getValue().getV()) + ", " + round(winner.getValue().getN()) + " )</p>");
                        if (winner == challenger_a) {
                            current_node.getChilds().get(0).getEdgeToParent().setPreferred(true);
                            current_node.setValue(value1);
                        } else {
                            current_node.getChilds().get(1).getEdgeToParent().setPreferred(true);
                            current_node.setValue(value2);
                        }
                    }
                } else if (current_node.getChilds().size() == 1) {
                    logAction("Данный узел является узлом решения.<br>");
                    Value value1 = new Value();

                    value1.add(current_node.getChilds().get(0).getValue());
                    value1.add(current_node.getChilds().get(0).getEdgeToParent().getValue());


                    logAction("Первый выбор - " + current_node.getChilds().get(0).getEdgeToParent().getDescription() + " :<br>");
                    logAction("Стоимость узла " + current_node.getChilds().get(0).getName() + " : ( " + round(current_node.getChilds().get(0).getValue().getV()) + ", " + round(current_node.getChilds().get(0).getValue().getN()) + " )<br>");
                    logAction("Стоимость перехода к узлу " + current_node.getChilds().get(0).getEdgeToParent().getName() + " : ( " + round(current_node.getChilds().get(0).getEdgeToParent().getValue().getV()) + ", " + round(current_node.getChilds().get(0).getEdgeToParent().getValue().getN()) + " )<br>");
                    logAction("Итоговая стоимость решения : ( " + round(value1.getV()) + ", " + round(value1.getN()) + ")<br>");

                    logAction("Т.к стоимость первого решения больше стоимости второго решения, стоимость узла "
                            + current_node.getName() + " становится равной ( " + round(value1.getV()) + ", " + round(value1.getN()) + " )</p>");
                    current_node.getChilds().get(0).getEdgeToParent().setPreferred(true);
                    current_node.setValue(value1);

                } else {
                    throw new RuntimeException("something weird happened");
                }

            } else if (current_node.getType() == NodeType.LOTTERY) {
                Value value = new Value();

                logAction("Данный узел является узлом лотереи.<br>");
                logAction("Цена узла равна E(" + current_node.getName() + ") = ");


                boolean first = true;
                for (Node child : current_node.getChilds()) {

                    if (first) {
                        first = false;
                    } else {
                        logAction(" + ");
                    }
                    logAction(child.getEdgeToParent().getProbability() + "* ("
                            + round(child.getEdgeToParent().getValue().getV()) + " + " + round(child.getValue().getV())
                            + ", "
                            + round(child.getEdgeToParent().getValue().getN()) + " + " + round(child.getValue().getN()) + ")");
                    Value child_value = new Value(child.getValue());
                    child_value.add(child.getEdgeToParent().getValue());
                    child_value.mul(child.getEdgeToParent().getProbability());
                    value.add(child_value);
                }
                logAction(" = (" + round(value.getV()) + ", " + round(value.getN()) + ")</p>");
                current_node.setValue(value);
            } else {
                //ERROR
            }
            logAction("<br><br>");
        }
    }

    private void logAction(String str){
        for (IStepLogger handler:getHandlersList()){
            handler.logStep(str);
        }
    }

    @Override
    final public void addComponent(IStepLogger handler){
        super.addComponent(handler);
        solver_.addComponent(handler);
    }
}
