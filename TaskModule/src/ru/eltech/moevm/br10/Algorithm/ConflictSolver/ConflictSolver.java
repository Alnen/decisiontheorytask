package ru.eltech.moevm.br10.algorithm.conflictsolver;

import ru.eltech.moevm.br10.utility.ListenerHandler;
import ru.eltech.moevm.br10.utility.listener.IStepLogger;

public abstract class ConflictSolver extends ListenerHandler<IStepLogger> implements IConflictSolver  {
    abstract public String getTypeString();

    protected void logAction(String str){
        for (IStepLogger handler:getHandlersList()){
            handler.logStep(str);
        }
    }
}