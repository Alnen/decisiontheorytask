package ru.eltech.moevm.br10.algorithm.conflictsolver;

import ru.eltech.moevm.br10.utility.Challenger;

public interface IConflictSolver {
    public Challenger chose (Challenger nodeA, Challenger nodeB);
}