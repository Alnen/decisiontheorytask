package ru.eltech.moevm.br10.algorithm.conflictsolver;

import ru.eltech.moevm.br10.utility.Challenger;

public class ConstantWConflictSolver extends ConflictSolver{
    private double w_;

    public ConstantWConflictSolver(){
        w_ = 1.0;
    }

    public ConstantWConflictSolver(double w){
        w_ = w;
    }

    private void setW (double w) {
        w_ = w;
    }

    @Override
    final public Challenger chose (Challenger challanger_a, Challenger challanger_b) {
        logAction("Для каждого из возможный решений расчитаем стоимость по формуле : ri = vi + (" + w_ + ")*si.<br>" );
        logAction("Для первого решения стоимость равна : " + (challanger_a.getValue().getV() + w_ * challanger_a.getValue().getN()) + "<br>");
        logAction("Для второго решения стоимость равна : " + (challanger_b.getValue().getV() + w_ * challanger_b.getValue().getN()) + "<br>");
        if ( challanger_a.getValue().getV() + w_ * challanger_a.getValue().getN() >=
                challanger_b.getValue().getV() + w_ * challanger_b.getValue().getN() )
        {
            logAction("Т.к стоимость первого решения больше стоимости второго решения, то выбираем первое решение.");
            return challanger_a;
        } else {
            logAction("Т.к стоимость второго решения больше стоимости первого решения, то выбираем второго решение.");
            return challanger_b;
        }
    }

    public String getTypeString(){
        return "Постоянный вес W = " + w_;
    }
}