package ru.eltech.moevm.br10.algorithm.conflictsolver;

import ru.eltech.moevm.br10.utility.Challenger;
import ru.eltech.moevm.br10.utility.ListenerHandler;
import ru.eltech.moevm.br10.utility.listener.IStepLogger;

import java.util.Random;

public class RandomConflictSolver extends ConflictSolver {
    private ListenerHandler<IStepLogger> listener_handler_;

    @Override
    final public Challenger chose (Challenger challanger_a, Challenger challanger_b) {
        Random random = new Random();
        if (random.nextDouble() >= 0.5) {
            logAction("Выбираем первое решение.");
            return challanger_a;
        } else {
            logAction("Выбираем второе решение.");
            return challanger_b;
        }
    }

    public String getTypeString(){
        return "Произвольный";
    }

}

