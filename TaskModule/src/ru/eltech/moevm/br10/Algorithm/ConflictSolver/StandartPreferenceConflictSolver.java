package ru.eltech.moevm.br10.algorithm.conflictsolver;

import ru.eltech.moevm.br10.utility.Challenger;

import java.util.ArrayList;

public class StandartPreferenceConflictSolver extends ConflictSolver {
    private ArrayList<String> preferred_list;

    public StandartPreferenceConflictSolver(){
        preferred_list = new ArrayList<String>();
    }

    public void addToPreferred (String node_name) {
        preferred_list.add(node_name);
    }

    public void deleteFromPreferred (String node_name) {
        preferred_list.remove(node_name);
    }

    @Override
    final public Challenger chose (Challenger challanger_a, Challenger challanger_b) {
        if (preferred_list.contains(challanger_a.getNode().getName())) {
            logAction("В соостветствии с выставленными приоритетами выбираем первое решение.");
            return challanger_a;
        } else {
            logAction("В соостветствии с выставленными приоритетами выбираем второе решение.");
            return challanger_b;
        }
    }

    public String getTypeString(){
        return "Заранее определенный приоритет";
    }
}