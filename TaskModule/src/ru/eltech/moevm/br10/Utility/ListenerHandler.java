package ru.eltech.moevm.br10.utility;

import java.util.ArrayList;
import java.util.List;

public class ListenerHandler<T> {
    private List<T>  listener_list_;

    public void addComponent (T handler) {
        listener_list_.add(handler);
    }

    protected ListenerHandler () {
        listener_list_ = new ArrayList<T>();
    }

    protected List<T> getHandlersList () {
        return listener_list_;
    }

}
