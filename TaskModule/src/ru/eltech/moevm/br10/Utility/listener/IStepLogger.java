package ru.eltech.moevm.br10.utility.listener;

public interface IStepLogger {
    public void logStep(String str);
}