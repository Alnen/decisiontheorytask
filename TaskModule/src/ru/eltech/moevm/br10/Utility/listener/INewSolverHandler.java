package ru.eltech.moevm.br10.utility.listener;

import ru.eltech.moevm.br10.algorithm.conflictsolver.ConflictSolver;

/**
 * Created by Alexandr on 02.04.2015.
 */
public interface INewSolverHandler {
    public void newSolver(ConflictSolver solver);
}
