package ru.eltech.moevm.br10.utility.listener;

import ru.eltech.moevm.br10.utility.Node;

public interface INodeChangeHanddler {
    public void valueChanged(Node node);
}
