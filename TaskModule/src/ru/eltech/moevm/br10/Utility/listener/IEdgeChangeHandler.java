package ru.eltech.moevm.br10.utility.listener;

import ru.eltech.moevm.br10.utility.Edge;

public interface IEdgeChangeHandler {
    public void setPrefered(Edge edge);
    public void valueChanged(Edge edge);
    public void probabilityChanged(Edge edge);
}
