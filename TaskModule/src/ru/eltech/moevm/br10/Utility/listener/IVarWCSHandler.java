package ru.eltech.moevm.br10.utility.listener;

/**
 * Created by Alexandr on 02.04.2015.
 */
public interface IVarWCSHandler {
    public void rminChanged();
    public void rmaxChanged();
    public void betaChanged();
}
