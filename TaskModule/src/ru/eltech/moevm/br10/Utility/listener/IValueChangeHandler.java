package ru.eltech.moevm.br10.utility.listener;

import ru.eltech.moevm.br10.utility.Value;

/**
 * Created by Alexandr on 30.03.2015.
 */
public interface IValueChangeHandler {
    public void valueChanged(Value value);
}
