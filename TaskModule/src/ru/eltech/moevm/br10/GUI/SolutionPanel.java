package ru.eltech.moevm.br10.GUI;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DSimple;
import info.monitorenter.gui.chart.views.ChartPanel;
import ru.eltech.moevm.br10.GUI.Utility.GUIElement;
import ru.eltech.moevm.br10.GUI.Utility.GroupableTableHeader;
import ru.eltech.moevm.br10.GUI.Utility.LastElCoords;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.util.Map;
import java.util.Vector;

public class SolutionPanel {
    private static Map vx_coords;
    private static Map edge_values;

    private LastElCoords last_el_coords;

    public SolutionPanel(JPanel panel)
    {
        last_el_coords = new LastElCoords();

        panel.setLayout(null);
        panel.setSize(760, 4700);
        panel.setPreferredSize(new Dimension(750, 4700));

        // Вычисления в случае двух атрибутов
        GUIElement.setTextHeader("Вычисления в случае двух атрибутов", 20, 300, 20, last_el_coords, panel);
        last_el_coords.updateValues(10, 400);
        GUIElement.setHtmlText600Px("./../resources/html/calc_with_two_attr.html", last_el_coords, panel);

        // Вычисления в случае линейных компромиссов
        GUIElement.setTextHeader("Вычисления в случае линейных компромиссов", 30, 400, 20, last_el_coords, panel);
        last_el_coords.updateValues(10, 400);
        GUIElement.setHtmlText600Px("./../resources/html/calc_linear_compromise.html", last_el_coords, panel);

        // Расчет полезностей решений.
        GUIElement.setTextHeader("Расчет полезностей решений", 30, 400, 20, last_el_coords, panel);
        last_el_coords.updateValues(10, 380);
        GUIElement.setHtmlText600Px("./../resources/html/calc_decision_utility_1.html", last_el_coords, panel);

        // Дерево решений
        createDecisionTree(panel);
        //

        // Расчет полезностей решений. (после дерева решений)
        last_el_coords.updateValues(30, 400);
        GUIElement.setHtmlText600Px("./../resources/html/calc_decision_utility_2.html", last_el_coords, panel);

        // «Цена» спасенной жизни.
        GUIElement.setTextHeader("\"Цена\" спасенной жизни", 30, 300, 20, last_el_coords, panel);
        last_el_coords.updateValues(10, 320);
        GUIElement.setHtmlText600Px("./../resources/html/cost_saved_life_1.html", last_el_coords, panel);

        // График зависимости p от w
        createPlotPW(panel);

        // «Цена» спасенной жизни. (Текст после графика)
        last_el_coords.updateValues(30, 90);
        GUIElement.setHtmlText600Px("./../resources/html/cost_saved_life_2.html", last_el_coords, panel);

        // Нелинейная полезность.
        GUIElement.setTextHeader("Нелинейная полезность", 30, 300, 20, last_el_coords, panel);
        last_el_coords.updateValues(10, 400);
        GUIElement.setHtmlText600Px("./../resources/html/noncapital_utility.html", last_el_coords, panel);

        // Нелинейная полезность в задаче о ядерном реакторе.
        GUIElement.setTextHeader("Нелинейная полезность в задаче о ядерном реакторе", 30, 500, 20, last_el_coords, panel);
        last_el_coords.updateValues(10, 320);
        GUIElement.setHtmlText600Px("./../resources/html/noncapital_utility_in_nuclear_reactor_task.html", last_el_coords, panel);

        // Таблица 1.4
        createCalcTable(panel);
    }

    public void createCalcTable(JPanel panel)
    {
        DefaultTableModel dm = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        dm.setDataVector(new Object[][]{
                        {"1", "3", "0.852", "", "0.891", "N", "0.951", "N"},
                        {"1", "2", "0.869", "T", "0.890", "T", "0.934", ""},
                        {"2", "3", "0.911", "", "0.914", "", "0.934", ""},
                        {"2", "3", "0.807", "", "0.855", "", "0.934", ""},
                        {"3", "4", "0.853", "C", "0.891", "C", "0.951", "C"},
                        {"3", "4", "0.807", "", "0.855", "", "0.934", "C"},
                        {"3", "5", "0.911", "A", "0.914", "A", "0.923", ""},
                        {"3", "4", "0.807", "C", "0.855", "C", "0.934", "C"},
                        {"3", "5", "0.484", "", "0.509", "", "0.574", ""},
                        {"4", "6", "0.864", "", "0.901", "", "0.959", ""},
                        {"4", "6", "0.318", "", "0.396", "", "0.577", ""},
                        {"4", "6", "0.818", "", "0.866", "", "0.942", ""},
                        {"4", "6", "0.273", "", "0.344", "", "0.518", ""},
                        {"4", "6", "0.818", "", "0.866", "", "0.942", ""},
                        {"4", "6", "0.273", "", "0.344", "", "0.518", ""},
                        {"5", "6", "1.0", "", "1.0", "", "1.0", ""},
                        {"5", "6", "0.182", "", "0.237", "", "0.38", ""},
                        {"5", "6", "0", "", "0", "", "0", ""},
                        {"5", "6", "1.0", "", "1.0", "", "1.0", ""},
                        {"5", "6", "0.182", "", "0.237", "", "0.38", ""},
                        {"5", "6", "0", "", "0", "", "0", ""},
                        {"6", "-", "-", "-", "-", "-", "-", "-"}
                },
                new Object[]{"Узел", "Следующий\nузел", "Полезность\nпри β=1", "Оптимальное\nрешение",
                        "Полезность\nпри β=2", "Оптимальное\nрешение", "Полезность\nпри β=10", "Оптимальное\nрешение"});
        JTable table = new JTable( dm ) {
            DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

            {
                renderRight.setHorizontalAlignment(SwingConstants.CENTER);
            }

            @Override
            public TableCellRenderer getCellRenderer (int arg0, int arg1) {
                return renderRight;
            }

            protected JTableHeader createDefaultTableHeader() {
                return new GroupableTableHeader(columnModel);
            }
        };
        TableColumnModel cm = table.getColumnModel();
        cm.getColumn(0).setMaxWidth(50);
        table.setPreferredSize(new Dimension(650,table.getRowHeight()*22));
        table.setSize(650, table.getRowHeight() * 22);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setSize(650, 415);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        last_el_coords.updateValues(30, scroll.getHeight());
        scroll.setLocation(panel.getWidth() / 2 - table.getWidth() / 2, last_el_coords.y);
        panel.add(scroll);
    }

    public void createPlotPW(JPanel panel)
    {
        Chart2D chart = new Chart2D();
        chart.getAxisX().getAxisTitle().setTitle("w");
        chart.getAxisY().getAxisTitle().setTitle("p");
        ITrace2D trace = new Trace2DSimple();
        trace.setName("");
        chart.addTrace(trace);
        double p = 0;
        int w = 0;
        trace.addPoint(0, 0);
        do {
            p = 1.0/(2.28-0.1*w);
            if (p > 0.01) {
                trace.addPoint(w, p);
            }
            w++;
        } while((p > 0.01)&&(p<=1));
        ChartPanel cp = new ChartPanel(chart);
        cp.setSize(400, 400);
        last_el_coords.updateValues(30, cp.getHeight());
        cp.setLocation(panel.getWidth() / 2 - cp.getWidth() / 2, last_el_coords.y);

        panel.add(cp);
    }

    public void createDecisionTree(JPanel panel)
    {
        Vector<Point2D> vector = new Vector();
        vector.add(new Point2D.Double(0, 0)); // N
        vector.add(new Point2D.Double(-1, 0)); // T
        vector.add(new Point2D.Double(-2, 0)); // C1
        vector.add(new Point2D.Double(10, 0)); // s1
        vector.add(new Point2D.Double(-2, 4)); // f1
        vector.add(new Point2D.Double(0, 0)); // p
        vector.add(new Point2D.Double(-2, 0)); // C2
        vector.add(new Point2D.Double(10, 0)); // s2
        vector.add(new Point2D.Double(-2, 4)); // f2
        vector.add(new Point2D.Double(-4, 0)); // A1
        vector.add(new Point2D.Double(16, 0)); // s3
        vector.add(new Point2D.Double(-2, 1)); // m1
        vector.add(new Point2D.Double(-6, 3)); // l1
        vector.add(new Point2D.Double(0, 0)); // n
        vector.add(new Point2D.Double(-2, 0)); // C3
        vector.add(new Point2D.Double(10, 0)); // s4
        vector.add(new Point2D.Double(-2, 4)); // f3
        vector.add(new Point2D.Double(-4, 0)); // A2
        vector.add(new Point2D.Double(16, 0)); // s5
        vector.add(new Point2D.Double(-2, 1)); // m2
        vector.add(new Point2D.Double(-6, 3)); // l2
        GUIElement.setDescriptionTree(panel, vector, last_el_coords, 10);
    }
}