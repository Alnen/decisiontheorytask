package ru.eltech.moevm.br10.GUI;

import ru.eltech.moevm.br10.GUI.Utility.ColumnGroup;
import ru.eltech.moevm.br10.GUI.Utility.GUIElement;
import ru.eltech.moevm.br10.GUI.Utility.GroupableTableHeader;
import ru.eltech.moevm.br10.GUI.Utility.LastElCoords;
import ru.eltech.moevm.br10.algorithm.Algorithm;
import ru.eltech.moevm.br10.algorithm.conflictsolver.ConflictSolver;
import ru.eltech.moevm.br10.utility.Edge;
import ru.eltech.moevm.br10.utility.Node;
import ru.eltech.moevm.br10.utility.listener.IEdgeChangeHandler;
import ru.eltech.moevm.br10.utility.listener.INodeChangeHanddler;
import ru.eltech.moevm.br10.utility.listener.IStepLogger;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Map;
import java.util.Vector;

public class ExamplePanel {

    private Node root_node;
    private JTable table;
    private Algorithm algorithm;
    private String algorithm_description;
    private JLabel description_area;

    //private static Map vx_coords;
    private static Map edge_values;
    private LastElCoords last_el_coords;

    public ExamplePanel(JPanel panel, Node root)
    {
        last_el_coords = new LastElCoords();
        panel.setLayout(null);
        panel.setSize(760, 1700);
        panel.setPreferredSize(new Dimension(750, 1700));

        createDecisionTable(panel);
        createButtonPanel(panel);
        createDecisionTextPane(panel);

        root_node = root;
        initTableFromTree(root);
        algorithm = new Algorithm(root);

        algorithm.addComponent(new IStepLogger() {
            @Override
            public void logStep(String str) {
                algorithm_description += str;
                System.out.println(str);
                description_area.setText("<html><body width=\"580\" height=\"500\">"
                        + algorithm_description
                        + "</body></html>");
            }
        });
        createDecisionTree(panel, root_node);
    }

    private void createDecisionTextPane(JPanel panel)
    {
        GUIElement.setTextHeader("Описание решения", 30, 300, 20, last_el_coords, panel);
        algorithm_description = "";
        description_area = new JLabel("<html><body width=\"580\" height=\"500\">"
                + algorithm_description
                + "</body></html>");
        description_area.setSize(580, 1400);
        description_area.setPreferredSize(new Dimension(580, 1400));
        JScrollPane scroll = new JScrollPane(description_area);
        scroll.setSize(600, 400);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        last_el_coords.updateValues(10, scroll.getHeight());
        scroll.setLocation(panel.getWidth() / 2 - scroll.getWidth() / 2, last_el_coords.y);
        panel.add(scroll);
    }

    private void createButtonPanel(JPanel panel) {
        JPanel jp = new JPanel();
        last_el_coords.updateValues(10, 35);
        jp.setSize(580, last_el_coords.height);
        jp.setLocation(panel.getWidth() / 2 - jp.getWidth() / 2, last_el_coords.y);
        JButton reset_btn = new JButton("Сбросить");
        reset_btn.setSize(150, 30);
        reset_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                algorithm.reset();
                algorithm_description = "";
                description_area.setText("<html><body width=\"580\" height=\"500\">"
                        + algorithm_description
                        + "</body></html>");
            }
        });
        jp.add(reset_btn);
        JButton one_step_btn = new JButton("Сделать один шаг");
        one_step_btn.setSize(150, 30);
        one_step_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                algorithm.proceedOneStep();
            }
        });
        jp.add(one_step_btn);
        JButton all_steps_btn = new JButton("Дойти до конца");
        all_steps_btn.setSize(150, 30);
        all_steps_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                algorithm.solve();
            }
        });
        jp.add(all_steps_btn);

        reset_btn.setLocation(jp.getWidth() / 2 - (20 * 2 + reset_btn.getWidth()
                + one_step_btn.getWidth() + all_steps_btn.getWidth()) / 2, 0);
        one_step_btn.setLocation(reset_btn.getX() + reset_btn.getWidth() + 20, 0);
        all_steps_btn.setLocation(all_steps_btn.getX() + all_steps_btn.getWidth() + 20, 0);

        panel.add(jp);
    }

    private void createDecisionTable(JPanel panel)
    {

        GUIElement.setTextHeader("Таблица оптимальных решений", 10, 400, 20, last_el_coords, panel);
        DefaultTableModel dm = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return (column == 0 || column == 1) ? false : true;
            }
        };

        dm.setDataVector(new Object[][]{
                        {"1", "2", "", "", "", "", "", ""},
                        {"1", "3.1", "", "", "", "", "", ""},
                        {"2", "3.2", "", "", "", "", "", ""},
                        {"2", "3.3", "", "", "", "", "", ""},
                        {"3.1", "4.1", "", "", "", "", "", ""},
                        {"3.2", "4.2", "", "", "", "", "", ""},
                        {"3.2", "5.1", "", "", "", "", "", ""},
                        {"3.3", "4.3", "", "", "", "", "", ""},
                        {"3.3", "5.2", "", "", "", "", "", ""},
                        {"4.1", "6.1", "", "", "", "", "", ""},
                        {"4.1", "6.2", "", "", "", "", "", ""},
                        {"4.2", "6.3", "", "", "", "", "", ""},
                        {"4.2", "6.4", "", "", "", "", "", ""},
                        {"5.1", "6.5", "", "", "", "", "", ""},
                        {"5.1", "6.6", "", "", "", "", "", ""},
                        {"5.1", "6.7", "", "", "", "", "", ""},
                        {"4.3", "6.8", "", "", "", "", "", ""},
                        {"4.3", "6.9", "", "", "", "", "", ""},
                        {"5.2", "6.10", "", "", "", "", "", ""},
                        {"5.2", "6.11", "", "", "", "", "", ""},
                        {"5.2", "6.12", "", "", "", "", "", ""},
                        {"6", "-", "-", "-", "-", "-", "-", "-", "-"}
                },
                new Object[]{"Узел", "Следующий\nузел", "Вероятность\nветви pj(hj)", "rj", "sj", "vj", "uj", "Оптимальное\nрешение"});
        table = new JTable( dm ) {
            DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

            {
                renderRight.setHorizontalAlignment(SwingConstants.CENTER);
            }

            @Override
            public TableCellRenderer getCellRenderer (int arg0, int arg1) {
                return renderRight;
            }

            protected JTableHeader createDefaultTableHeader() {
                return new GroupableTableHeader(columnModel);
            }
        };
        TableColumnModel cm = table.getColumnModel();
        cm.getColumn(0).setMaxWidth(80);
        ColumnGroup g_1 = new ColumnGroup("Стоимость ветви");
        g_1.add(cm.getColumn(3));
        g_1.add(cm.getColumn(4));
        ColumnGroup g_2 = new ColumnGroup("Ожидаемый доход в узле");
        g_2.add(cm.getColumn(5));
        g_2.add(cm.getColumn(6));

        GroupableTableHeader header = (GroupableTableHeader)table.getTableHeader();
        header.addColumnGroup(g_1);
        header.addColumnGroup(g_2);

        table.setPreferredSize(new Dimension(700, table.getRowHeight() * 22));
        table.setSize(700, table.getRowHeight() * 22);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setSize(700, 415);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        last_el_coords.updateValues(10, scroll.getHeight());
        scroll.setLocation(panel.getWidth() / 2 - table.getWidth() / 2, last_el_coords.y);
        panel.add(scroll);

    }

    private void initTableFromTree(Node node)
    {
        root_node = node;
        ArrayList<Node> node_stack = new ArrayList<Node>();
        node_stack.add(root_node);
        while (!node_stack.isEmpty())
        {
            Node current_root_node = node_stack.remove(0);
            current_root_node.addComponent(new INodeChangeHanddler() {
                @Override
                public void valueChanged(Node node) {
                    for (int i = 0; i < table.getRowCount() - 1; ++i) {
                        if (table.getModel().getValueAt(i, 1).toString().equals(node.getName())) {
                            table.getModel().setValueAt(Algorithm.round(node.getValue().getV()), i, 5);
                            table.getModel().setValueAt(Algorithm.round(node.getValue().getN()), i, 6);
                            break;
                        }
                    }
                    //edge_values.replace("");
                }
            });
            if (current_root_node.getEdgeToParent() != null)
            current_root_node.getEdgeToParent().addComponent(new IEdgeChangeHandler() {
                @Override
                public void setPrefered(Edge edge) {
                    for (int i = 0; i < table.getRowCount()-1; ++i) {
                        if ((table.getModel().getValueAt(i, 0).toString()+"-"+table.getModel().getValueAt(i, 1).toString()).equals(edge.getId())) {
                            if(edge.getPreferred()) {
                                table.getModel().setValueAt(edge.getName(),i, 7);
                            } else {
                                table.getModel().setValueAt("",i, 7);
                            }
                            break;
                        }
                    }
                }

                @Override
                public void valueChanged(Edge edge) {
                    for (int i = 0; i < table.getRowCount()-1; ++i) {
                        if ((table.getModel().getValueAt(i, 0).toString()+"-"+table.getModel().getValueAt(i, 1).toString()).equals(edge.getId())) {
                            //System.out.println(i+":"+edge.getProbability());
                            table.getModel().setValueAt(edge.getValue().getV(),i, 3);
                            table.getModel().setValueAt(edge.getValue().getN(),i, 4);
                            break;
                        }
                    }
                }

                @Override
                public void probabilityChanged(Edge edge) {
                    for (int i = 0; i < table.getRowCount()-1; ++i) {
                        if ((edge != null)&&((table.getModel().getValueAt(i, 0).toString()+"-"+table.getModel().getValueAt(i, 1).toString()).equals(edge.getId()))) {
                            //System.out.println(i+":"+edge.getProbability());
                            table.getModel().setValueAt(edge.getProbability(), i, 2);
                            break;
                        }
                    }
                }
            });
            for (Node child: current_root_node.getChilds()) {
                node_stack.add(child);
            }
            for (int i = 0; i < table.getRowCount()-1; ++i)
            {
                if ((current_root_node.getEdgeToParent() != null)) {
                    System.out.println(i+":"+current_root_node.getEdgeToParent().getId());
                } else {
                    System.out.println(i+": is null");
                }
                if ((current_root_node.getEdgeToParent() != null)&&((table.getModel().getValueAt(i, 0).toString()+"-" +table.getModel().getValueAt(i, 1).toString()).equals(current_root_node.getEdgeToParent().getId()))) {
                    System.out.println(i+":"+current_root_node.getEdgeToParent().getProbability());
                    table.getModel().setValueAt(current_root_node.getEdgeToParent().getProbability(), i, 2);
                    table.getModel().setValueAt(current_root_node.getEdgeToParent().getValue().getV(),i, 3);
                    table.getModel().setValueAt(current_root_node.getEdgeToParent().getValue().getN(),i, 4);
                    break;
                }
            }
        }
    }

    public void setSolver(ConflictSolver solver) {
        algorithm.setSolver(solver);
    }

    public void createDecisionTree(JPanel panel, Node tree)
    {
        Vector<Point2D> vector = new Vector<Point2D>(21);
        Vector<Node> nodes = new Vector<Node>(21);
        Node node = tree.getChilds().get(1);
        nodes.add(node); // N
        nodes.add(tree.getChilds().get(0)); // T
        node = node.getChilds().get(0);
        nodes.add(node); // C1
        nodes.add(node.getChilds().get(0)); // s1
        nodes.add(node.getChilds().get(1)); // f1
        node = tree.getChilds().get(0).getChilds().get(0);
        nodes.add(node); // p
        node = node.getChilds().get(0);
        nodes.add(node); // C2
        nodes.add(node.getChilds().get(0)); // s2
        nodes.add(node.getChilds().get(1)); // f2
        node = tree.getChilds().get(0).getChilds().get(0).getChilds().get(1);
        nodes.add(node); // A1
        nodes.add(node.getChilds().get(0)); // s3
        nodes.add(node.getChilds().get(1)); // m1
        nodes.add(node.getChilds().get(2)); // l1
        node = tree.getChilds().get(0).getChilds().get(1);
        nodes.add(node); // n
        node = node.getChilds().get(0);
        nodes.add(node); // C3
        nodes.add(node.getChilds().get(0)); // s4
        nodes.add(node.getChilds().get(1)); // f3
        node = tree.getChilds().get(0).getChilds().get(1).getChilds().get(1);
        nodes.add(node); // A2
        nodes.add(node.getChilds().get(0)); // s5
        nodes.add(node.getChilds().get(1)); // m2
        nodes.add(node.getChilds().get(2)); // l2

        for (Node child: nodes)
        {
            if (child.getEdgeToParent() != null) {
                vector.add(new Point2D.Double(child.getEdgeToParent().getValue().getV(), child.getEdgeToParent().getValue().getN()));
            }
        }

        GUIElement.setTextHeader("Дерево решений", 30, 300, 20, last_el_coords, panel);
        GUIElement.setDescriptionTree(panel, vector, last_el_coords, 10);
    }
}
