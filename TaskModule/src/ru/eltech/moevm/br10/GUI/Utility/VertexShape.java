package ru.eltech.moevm.br10.GUI.Utility;

/**
 * Created by Alexandr on 01.04.2015.
 */
public class VertexShape
{
    public String name;
    public String shape;

    public VertexShape(String name, String shape)
    {
        this.name = name;
        this.shape = shape;
    }
}