package ru.eltech.moevm.br10;

import ru.eltech.moevm.br10.GUI.DescriptionPanel;
import ru.eltech.moevm.br10.GUI.ExamplePanel;
import ru.eltech.moevm.br10.GUI.InputPanel;
import ru.eltech.moevm.br10.GUI.SolutionPanel;
import ru.eltech.moevm.br10.algorithm.conflictsolver.ConflictSolver;
import ru.eltech.moevm.br10.utility.Edge;
import ru.eltech.moevm.br10.utility.Node;
import ru.eltech.moevm.br10.utility.NodeType;
import ru.eltech.moevm.br10.utility.listener.INewSolverHandler;
import ru.spb.beawers.modules.ITaskModule;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NuclearReactorWith2CriteriaTaskModule implements ITaskModule {

    private DescriptionPanel dp;
    private SolutionPanel sp;
    private InputPanel ip;
    private ExamplePanel ep;
    private Node root_node;

    public NuclearReactorWith2CriteriaTaskModule(){
        dp = null;
        sp = null;
        ip = null;
        ep = null;
        root_node = getTree();
    }

    private Node getTree(){
        Node node1  = new Node("1", NodeType.DECISION);
        Node node2  = new Node("2", NodeType.LOTTERY);
        Node node31 = new Node("3.1", NodeType.DECISION);
        Node node32 = new Node("3.2", NodeType.DECISION);
        Node node33 = new Node("3.3", NodeType.DECISION);
        Node node41 = new Node("4.1", NodeType.LOTTERY);
        Node node42 = new Node("4.2", NodeType.LOTTERY);
        Node node43 = new Node("4.3", NodeType.LOTTERY);
        Node node51 = new Node("5.1", NodeType.LOTTERY);
        Node node52 = new Node("5.2", NodeType.LOTTERY);

        Node node61 = new Node("6.1", NodeType.ACCIDENT);
        Node node62 = new Node("6.2", NodeType.ACCIDENT);
        Node node63 = new Node("6.3", NodeType.ACCIDENT);
        Node node64 = new Node("6.4", NodeType.ACCIDENT);
        Node node65 = new Node("6.5", NodeType.ACCIDENT);
        Node node66 = new Node("6.6", NodeType.ACCIDENT);
        Node node67 = new Node("6.7", NodeType.ACCIDENT);
        Node node68 = new Node("6.8", NodeType.ACCIDENT);
        Node node69 = new Node("6.9",   NodeType.ACCIDENT);
        Node node610 = new Node("6.10", NodeType.ACCIDENT);
        Node node611 = new Node("6.11", NodeType.ACCIDENT);
        Node node612 = new Node("6.12", NodeType.ACCIDENT);

        Edge edge2 = new Edge("1-2", "T","DT");
        Edge edge31 = new Edge("1-3.1", "N","DN");
        Edge edge32 = new Edge("2-3.2","p","Dp");
        Edge edge33 = new Edge("2-3.3","n","Dn");

        Edge edge41 = new Edge("3.1-4.1", "C1","DC1");
        Edge edge42 = new Edge("3.2-4.2", "C2","DC2");
        Edge edge43 = new Edge("3.2-5.1", "A1","DA1");
        Edge edge51 = new Edge("3.3-4.3", "C3","DC3");
        Edge edge52 = new Edge("3.3-5.2", "A2","DA2");

        Edge edge61 = new Edge("4.1-6.1", "s1","Ds1");
        Edge edge62 = new Edge("4.1-6.2", "f1","Df1");
        Edge edge63 = new Edge("4.2-6.3", "s2","Ds2");
        Edge edge64 = new Edge("4.2-6.4","f2","Df2");
        Edge edge65 = new Edge("5.1-6.5", "s3","Ds3");
        Edge edge66 = new Edge("5.1-6.6", "m3","Dm3");
        Edge edge67 = new Edge("5.1-6.7", "l3","Dl3");
        Edge edge68 = new Edge("4.3-6.8", "s4","Ds4");
        Edge edge69 = new Edge("4.3-6.9", "f4","Df4");
        Edge edge610 = new Edge("5.2-6.10", "s5","Ds5");
        Edge edge611 = new Edge("5.2-6.11", "m5","Dm5");
        Edge edge612 = new Edge("5.2-6.12", "l5","Dl5");
        /*Edge edge2 = new Edge("T","DT",Double.parseDouble((String)table.getModel().getValueAt(0, 2)),Double.parseDouble((String) table.getModel().getValueAt(0, 3)));
        Edge edge31 = new Edge("N","DN",Double.parseDouble((String)table.getModel().getValueAt(1, 2)),Double.parseDouble((String) table.getModel().getValueAt(1, 3)));
        Edge edge32 = new Edge("p","Dp",Double.parseDouble((String)table.getModel().getValueAt(2, 2)),Double.parseDouble((String) table.getModel().getValueAt(2, 3)),Double.parseDouble((String)table.getModel().getValueAt(2, 1)));
        Edge edge33 = new Edge("n","Dn",Double.parseDouble((String)table.getModel().getValueAt(3, 2)),Double.parseDouble((String) table.getModel().getValueAt(3, 3)),Double.parseDouble((String)table.getModel().getValueAt(3, 1)));

        Edge edge41 = new Edge("C1","DC1",Double.parseDouble((String)table.getModel().getValueAt(4, 2)),Double.parseDouble((String) table.getModel().getValueAt(4, 3)));
        Edge edge42 = new Edge("C2","DC2",Double.parseDouble((String)table.getModel().getValueAt(5, 2)),Double.parseDouble((String) table.getModel().getValueAt(5, 3)));
        Edge edge43 = new Edge("A1","DA1",Double.parseDouble((String)table.getModel().getValueAt(7, 2)),Double.parseDouble((String) table.getModel().getValueAt(7, 3)));
        Edge edge51 = new Edge("C3","DC3",Double.parseDouble((String)table.getModel().getValueAt(6, 2)),Double.parseDouble((String) table.getModel().getValueAt(6, 3)));
        Edge edge52 = new Edge("A2","DA2",Double.parseDouble((String)table.getModel().getValueAt(8, 2)),Double.parseDouble((String) table.getModel().getValueAt(8, 3)));

        Edge edge61 = new Edge("s1","Ds1",
                            Double.parseDouble((String)table.getModel().getValueAt(9, 2)),
                            Double.parseDouble((String) table.getModel().getValueAt(9, 3)),
                            Double.parseDouble((String)table.getModel().getValueAt(9, 1)));
        Edge edge62 = new Edge("f1","Df1",Double.parseDouble((String)table.getModel().getValueAt(10, 2)),Double.parseDouble((String) table.getModel().getValueAt(10, 3)),Double.parseDouble((String)table.getModel().getValueAt(10, 1)));
        Edge edge63 = new Edge("s2","Ds2",Double.parseDouble((String)table.getModel().getValueAt(11, 2)),Double.parseDouble((String) table.getModel().getValueAt(11, 3)),Double.parseDouble((String)table.getModel().getValueAt(11, 1)));
        Edge edge64 = new Edge("f2","Df2",Double.parseDouble((String)table.getModel().getValueAt(12, 2)),Double.parseDouble((String) table.getModel().getValueAt(12, 3)),Double.parseDouble((String)table.getModel().getValueAt(12, 1)));
        Edge edge65 = new Edge("s3","Ds3",Double.parseDouble((String)table.getModel().getValueAt(13, 2)),Double.parseDouble((String) table.getModel().getValueAt(13, 3)),Double.parseDouble((String)table.getModel().getValueAt(13, 1)));
        Edge edge66 = new Edge("m3","Dm3",Double.parseDouble((String)table.getModel().getValueAt(14, 2)),Double.parseDouble((String) table.getModel().getValueAt(14, 3)),Double.parseDouble((String)table.getModel().getValueAt(14, 1)));
        Edge edge67 = new Edge("l3","Dl3",Double.parseDouble((String)table.getModel().getValueAt(15, 2)),Double.parseDouble((String) table.getModel().getValueAt(15, 3)),Double.parseDouble((String)table.getModel().getValueAt(15, 1)));
        Edge edge68 = new Edge("s4","Ds4",Double.parseDouble((String)table.getModel().getValueAt(16, 2)),Double.parseDouble((String) table.getModel().getValueAt(16, 3)),Double.parseDouble((String)table.getModel().getValueAt(16, 1)));
        Edge edge69 = new Edge("f4","Df4",Double.parseDouble((String)table.getModel().getValueAt(17, 2)),Double.parseDouble((String) table.getModel().getValueAt(17, 3)),Double.parseDouble((String)table.getModel().getValueAt(17, 1)));
        Edge edge610 = new Edge("s5","Ds5",Double.parseDouble((String)table.getModel().getValueAt(18, 2)),Double.parseDouble((String) table.getModel().getValueAt(18, 3)),Double.parseDouble((String)table.getModel().getValueAt(18, 1)));
        Edge edge611 = new Edge("m5","Dm5",Double.parseDouble((String)table.getModel().getValueAt(19, 2)),Double.parseDouble((String) table.getModel().getValueAt(19, 3)),Double.parseDouble((String)table.getModel().getValueAt(19, 1)));
        Edge edge612 = new Edge("l5","Dl5",Double.parseDouble((String)table.getModel().getValueAt(20, 2)),Double.parseDouble((String) table.getModel().getValueAt(20, 3)),Double.parseDouble((String)table.getModel().getValueAt(20, 1)));
        */

        node2.setEdgeToParent(edge2);
        node31.setEdgeToParent(edge31);
        node32.setEdgeToParent(edge32);
        node33.setEdgeToParent(edge33);
        node41.setEdgeToParent(edge41);
        node42.setEdgeToParent(edge42);
        node43.setEdgeToParent(edge43);
        node51.setEdgeToParent(edge51);
        node52.setEdgeToParent(edge52);
        node61.setEdgeToParent(edge61);
        node62.setEdgeToParent(edge62);
        node63.setEdgeToParent(edge63);
        node64.setEdgeToParent(edge64);
        node65.setEdgeToParent(edge65);
        node66.setEdgeToParent(edge66);
        node67.setEdgeToParent(edge67);
        node68.setEdgeToParent(edge68);
        node69.setEdgeToParent(edge69);
        node610.setEdgeToParent(edge610);
        node611.setEdgeToParent(edge611);
        node612.setEdgeToParent(edge612);

        node1.addChild(node2);
        node1.addChild(node31);

        node2.addChild(node32);
        node2.addChild(node33);

        node31.addChild(node41);

        node32.addChild(node42);
        node32.addChild(node51);

        node33.addChild(node43);
        node33.addChild(node52);

        node41.addChild(node61);
        node41.addChild(node62);

        node42.addChild(node63);
        node42.addChild(node64);

        node51.addChild(node65);
        node51.addChild(node66);
        node51.addChild(node67);

        node43.addChild(node68);
        node43.addChild(node69);

        node52.addChild(node610);
        node52.addChild(node611);
        node52.addChild(node612);

        return  node1;
    }

    @Override
    public String getTitle(){
        return "Задача о ядерном реакторе с 2мя критериями";
    }

    @Override
    public void initDescriptionPanel(JPanel panel){
        dp = new DescriptionPanel(panel);

    }

    @Override
    public void initSolutionPanel(JPanel panel){
        sp = new SolutionPanel(panel);
    }

    @Override
    public void initInputPanel(JPanel panel) {
        ip = new InputPanel(panel, root_node);
        if (ep != null) {
            ip.addComponent(new INewSolverHandler() {
                @Override
                public void newSolver(ConflictSolver solver) {
                    ep.setSolver(solver);
                }
            });
        }
    }

    @Override
    public void initExamplePanel(JPanel panel){
        ep = new ExamplePanel(panel, root_node);
    }

    @Override
    public ActionListener getPressSaveListener() throws IllegalArgumentException{
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        };
    }

    @Override
    public ActionListener getPressLoadListener() throws IllegalArgumentException{
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        };
    }

    @Override
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException{
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        };
    }
}
