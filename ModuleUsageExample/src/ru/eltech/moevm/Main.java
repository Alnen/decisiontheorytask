package ru.eltech.moevm;

import ru.eltech.moevm.br10.NuclearReactorWith2CriteriaTaskModule;

import javax.swing.*;
import java.awt.*;

class NuclearReactorModule extends JFrame
{
    static int i = 0;

    public NuclearReactorModule()
    {
        super("Задача о ядерном реакторе с 2 критериями");

        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        setSize(800, 600);
        setMinimumSize(new Dimension(800, 600));
        setMaximumSize(new Dimension(800, 600));

        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Font font = new Font("Verdana", Font.PLAIN, 10);
        final JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.setFont(font);
        tabbedPane.setLocation(2, 0);
        tabbedPane.setSize(getWidth() - 20, getHeight() - 40);

        JPanel description_panel = new JPanel();
        JPanel solution_panel = new JPanel();
        JPanel input_panel = new JPanel();
        JPanel example_panel = new JPanel();

        JScrollPane scroll = new JScrollPane(description_panel);
        //scroll.setViewportView(description_panel);
        scroll.setSize(new Dimension(tabbedPane.getWidth() - 20, tabbedPane.getHeight() - 10));
        //scroll.setPreferredSize(new Dimension(tabbedPane.getWidth() - 20, tabbedPane.getHeight() - 10));
        tabbedPane.addTab("DescriptionPanel", scroll);
        scroll = new JScrollPane(solution_panel);
        scroll.setSize(tabbedPane.getWidth() - 20, tabbedPane.getHeight() - 10);
        tabbedPane.addTab("SolutionPanel", scroll);
        scroll = new JScrollPane(input_panel);
        scroll.setSize(tabbedPane.getWidth() - 20, tabbedPane.getHeight() - 10);
        tabbedPane.addTab("InputPanel", scroll);
        scroll = new JScrollPane(example_panel);
        scroll.setSize(tabbedPane.getWidth() - 20, tabbedPane.getHeight() - 10);
        tabbedPane.addTab("ExamplePanel", scroll);

        NuclearReactorWith2CriteriaTaskModule module = new NuclearReactorWith2CriteriaTaskModule();
        module.initDescriptionPanel(description_panel);
        module.initExamplePanel(example_panel);
        module.initInputPanel(input_panel);
        module.initSolutionPanel(solution_panel);

        getContentPane().add(tabbedPane);
        setVisible(true);
    }

}


public class Main {

    public static void main(String[] args)
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new NuclearReactorModule();
            }
        });
    }
}
